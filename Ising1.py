#!/usr/bin/env/ python

import numpy as np
import matplotlib.pyplot as plt
import Ising_functions as Is
from datetime import datetime



shape = (64, 64)
N = shape[0] * 1.
# Interaction (ferromagnetic if positive, antiferromagnetic if negative)
J = 1


#Calculating obervables
Y = np.arange(2.0, 2.5 ,.01)
Energy = []
Mag = []
Heat_Cap =[]
Sus = []

t_equi = 5000
t_stable = 20
for temperature in Y:
    spins = np.random.choice([-1, 1], size=shape)
    t = 0
    Energy_of_all_configs = []
    mag_of_all_configs = []
    Energy_sqrd_of_all_configs = []
    mag_sqrd_of_all_configs = []

    while t <= t_equi:
		#spins = Is.update(spins, T, n)   #Update with M-H algorithm  
		spins = Is.get_cluster(spins, temperature, [], [], N)   #Update with Wolff algorithm 
		t += 1
		 


    while t > t_equi and t <= t_stable + t_equi:
		energy_of_config = Is.get_energy(spins, 1)/(N*N) #Average Energy of a given configuration
		mag_of_config = np.abs(np.sum(spins))/(N*N)#Average Magnetisation of a given configuration
		


#We store the average of each observable for each configuration in order to take an average later and reduce fluctuations 
		Energy_of_all_configs.append(energy_of_config) 
		mag_of_all_configs.append(mag_of_config)
		Energy_sqrd_of_all_configs.append(energy_of_config**2)
   		mag_sqrd_of_all_configs.append(mag_of_config**2)
	
	   	
		#spins = Is.update(spins, T, n)    #Update with M-H algorithm  
		spins = Is.get_cluster(spins, temperature, [], [], N)#Update with Wolff algorithm
	  	t += 1

    
 
    print(temperature)
    E = np.sum(Energy_of_all_configs)/t_stable
    M = np.sum(mag_of_all_configs)/t_stable
    E_sqrd = np.sum(Energy_sqrd_of_all_configs)/t_stable
    Mag_sqrd = np.sum(mag_sqrd_of_all_configs)/t_stable
    
    heat_capacity = (E_sqrd - E**2)/((temperature*1.)**2)
    Susceptibility = (Mag_sqrd - M**2)/(temperature*1.)
    
    Energy.append(E)
    Mag.append(M)
    Heat_Cap.append(heat_capacity)
    Sus.append(Susceptibility)
 




plt.plot(Y, Energy, 'o')
plt.title('Energy Vs Temperature')
plt.xlabel('Temperature')
plt.ylabel('Energy')
plt.show()

plt.plot(Y, Mag, 'o')
plt.title('Magnetisation Vs Temperature')
plt.xlabel('Temperature')
plt.ylabel('Magnetisation')
plt.show()

plt.plot(Y, Heat_Cap, 'o')
plt.title('Specific Heat Vs Temperature')
plt.xlabel('Temperature')
plt.ylabel('Specific Heat')
plt.show()


plt.plot(Y, Sus, 'o')
plt.title('Magnetic Susceptibility Vs Temperature')
plt.xlabel('Temperature')
plt.ylabel('Susceptibility')
plt.show()



