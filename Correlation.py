#!/usr/bin/env/ python
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import Ising_functions as Is




n = 20 # Lattice size
steps = 6000
shape = (n, n)

temperature = [ 2.2]

# Different values of the separation
rs = np.arange(1, 10, 1)			


def Correlation(lattice, icorr, jcorr, corr_r, n):
	Sn_corr = lattice[(icorr - corr_r) % n, jcorr] + lattice[(icorr + corr_r) % n, jcorr] + lattice[icorr, (jcorr - corr_r) % n] + lattice[icorr, (jcorr + corr_r) % n]
	Si = lattice[icorr, jcorr]
	SiSj = Si * Sn_corr / 4.0
	Sis.append(Si)
	SiSjs.append(SiSj)
		    
	return Sis, SiSjs


for T in temperature:
	corr_funcs = []
	spins = np.random.choice([-1, 1], size=shape)

	for r in rs:
		t = 0 
		Sis = []
		SiSjs = []
		while t < steps:
				Sis, SiSjs = Correlation(spins, 10, 10, r, n)
				spins = Is.update(spins, T, n)
				#spins = Is.get_cluster(spins, temperature, [], [], n)
				t = t+1
		
		print(r)
		Si_avg = sum(Sis) / steps
		SiSj_avg = sum(SiSjs) / steps
	 	corr_func = np.abs(SiSj_avg - Si_avg ** 2)
		corr_funcs.append(corr_func)
		analytic =  corr_funcs[0] * rs**(-.25)
	plt.plot(rs, corr_funcs, 'o-', label='T = {0}'.format(T))



plt.plot(rs, analytic, label = 'Fit')
plt.xlabel('Separation between spins', fontsize =16)
plt.ylabel('Correlation Function', fontsize = 16)
plt.legend()
plt.show()

		
    




	
            
		

