#!/usr/bin/env/ python

import numpy as np
import matplotlib.pyplot as plt
import Ising_functions as Is


#Initialise lattice and other variables
IACs = []
Ns = np.array([  10])
temperature = 2.3

#Set up functions 
'''
Calculates the autocorrelation curve of M, then returns the integrated 
autocorrelation time (IAC).
'''
def find_auto_curve(M):
	autocorr = auto_corr(M)    
	return 1 + 2*np.sum(autocorr), autocorr

def auto_corr(M):
#   The autocorrelation has to be truncated at some point so there are enough
#   data points constructing each lag. Let cut_off be this cutoff
	auto_corr = np.zeros(cut_off-1)
	mu = np.mean(M)
	for s in range(1,cut_off-1):
		auto_corr[s] = np.mean( (M[:-s]-mu) * (M[s:]-mu) ) / np.var(M)
	auto_corr[0] = 1
	return auto_corr

'''
Plots the autocorrelation curves and calculates the IAC for Magnetisations for given configurations
'''
def compare_IAC(Ms,labels):
	for ind,M in enumerate(Ms):
		IAC,G = find_auto_curve(M)
		IACs.append(IAC)
		plt.plot(np.arange(len(G)),G,label="{}: IAC = {:.2f}".\
		                                    format(labels[ind],IAC))

	   
	print(G[-1])
	plt.legend(loc='best',fontsize=14)
	
	    
	  


#Main Part of the code
'''
Brings our lattice to a steady state, then stores the magnetisation for the next 400 configurations. The lattice is updated using both the Metropolis-Hasting algrithm and the Wolff algorithm 
'''
for n in Ns:
	shape = (n, n)
	N = shape[0] * 1.
	M_m = []
	M_w = []
	t = 0
	cut_off = 80

	spins_metro =  np.random.choice([-1, 1], size=shape)
	spins_wolff =  np.random.choice([-1, 1], size=shape)

	while t <= 400:
		spins_metro = Is.update(spins_metro, temperature, N) 
		spins_wolff = Is.get_cluster(spins_wolff, temperature, [], [], N)   
		t += 1


	#Once in equilibrium, store magnetisation of configurations
	while t >=  400 and t < 2000:
		mag_of_config_met = np.abs(np.sum(spins_metro))/N**2
		M_m.append(mag_of_config_met)
		spins_metro = Is.update(spins_metro, temperature, N)


		mag_of_config_w = np.abs(np.sum(spins_wolff))/N**2
		M_w.append(mag_of_config_w)
		spins_wolff = Is.get_cluster(spins_wolff, temperature, [], [], N)  

		t = t + 1
	'''	
	Call functions to calculate the IAC based on magnetisations of configs.
	When comparing autocorrelation of Metropolis and Wolff comment the first line and uncomment second line-also it's advisable to only run this for one lattice size at a time, this is done by only putting one value in Ns. 
	When comparing autocorrelation for lattices of different sizes, comment out second line, and uncomment first. Call M_m for metropolis, and M_w for Wolff
	'''	
	#compare_IAC([M_m],labels=['Metropolis n =%s' %n ])	
	compare_IAC([M_m, M_w],labels=['Metropolis', 'Wolff' ])   


   
#Plots
plt.title('Autocorrelation Function')
plt.xlabel('C(t)')
plt.ylabel('MCS')
plt.show()
plt.close()


if len(Ns) > 1:
	IACs = np.asarray(IACs)
	p = np.polyfit(Ns, IACs, 1)
	fit = np.poly1d(np.polyfit(Ns, IACs, 1))
	print(fit)
	plt.plot(Ns, IACs, 'o')
	plt.plot(np.unique(Ns), np.poly1d(np.polyfit(Ns, IACs, 1))(np.unique(Ns))  )
	plt.title('Autocorrelation time Vs Lattice size')
	plt.xlabel('L')
	plt.ylabel('IAC')
	plt.show()









