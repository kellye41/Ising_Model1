#!/usr/bin/env/ python

import numpy as np
import matplotlib.pyplot as plt



def probability(energy1, energy2, temperature):
    return np.exp((energy1 - energy2) / temperature)

def get_energy(spins, J):
     h = -np.sum(
     J * spins * np.roll(spins, 1, axis=0) +
     J * spins * np.roll(spins, -1, axis=0) +
     J * spins * np.roll(spins, 1, axis=1) +
     J * spins * np.roll(spins, -1, axis=1)
     )/2 
     return h


def update(spins, temperature, N):
	
	iter1 = 0
	while iter1 <= N*N:
		spins_new = np.copy(spins)
		i = np.random.randint(spins.shape[0])
		j = np.random.randint(spins.shape[1])
		spins_new[i, j] *= -1
	 
		current_energy = get_energy(spins, 1)
		new_energy = get_energy(spins_new, 1)
	      
		if probability(current_energy, new_energy, temperature)>np.random.random():
			spins = spins_new
	 	else:
			spins =  spins
		iter1 = iter1 + 1
	return spins

#Here one time step equals one spin site updated.
def update2(spins, temperature):
	spins_new = np.copy(spins)
	i = np.random.randint(spins.shape[0])
	j = np.random.randint(spins.shape[1])
	spins_new[i, j] *= -1
 
	current_energy = get_energy(spins, 1)
	new_energy = get_energy(spins_new, 1)
      
	if probability(current_energy, new_energy, temperature) > np.random.random():
		return spins_new
 	else:
		return spins





#For Wolff Algorithm 


def get_neighbours(i, j, N):
	return [([(i-1)%N, j ]),([(i+1)%N, j]),([i, (j-1)%N]),([i, (j+1)%N])]
'''
Selects an initial spin, if any of it's neighbouring spins have the same value, they are added to a cluster with probabily p = exp(-2/temp). If a spin is added, then they're neightbours are visited, and the above procedure is repeated. This is done until no more spins are added. Then the whole cluster off spins is flipped.
'''
def get_cluster(spins, temp, cluster, visited, size):
	i = np.random.randint(spins.shape[0])
	j = np.random.randint(spins.shape[1])
	cluster.append([i, j])
        for h in cluster:
		if h not in visited:
			
			y = h[0]
			w = h[1]
			neighbours = get_neighbours(y, w, size)

			for n in neighbours:
				k = n[0]
				l = n[1]
				if n not in visited:
					if spins[i, j] == spins[k, l]:
						if 1 - np.exp(-2/(temp*1.)) > np.random.random():
							if n not in cluster:
								cluster.append(n)
								
						
						else:
							visited.append(n)
							
						
					else:
						visited.append(n)
						
		
			visited.append(h)

	
	for m in cluster:
		a = m[0]
		b = m[1]
		spins[a, b] *= -1 
	return(spins)



#Plotting module-gives a visual representation of Ising module
def myplot(A, file):
	n, m =A.shape

	f = open(file, 'w')
	f.write("P1 \n")
	f.write("# myplot module \n")
	st=str(n) + " " +str(m) + "\n"
	f.write(st)

	B = A.astype(bool)
	np.savetxt(f, B, fmt="%i" )

