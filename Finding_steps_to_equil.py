#!/usr/bin/env/ python

import numpy as np
import matplotlib.pyplot as plt
import Ising_functions as Is



shape = (8, 8)
size = shape[0]
N = shape[0] * 1.
temperature = .7 # Temperature (in units of energy)


t = 0
spins = np.random.choice([-1, 1], size=shape)
time = []
Mag = []


while t <= 200:

	spins = Is.get_cluster(spins, temperature, [], [], N) 
	#spins = Is.update(spins, temperature, N ) 
        #spins = Is.update2(spins, temperature)
	mag_of_config = np.abs(np.sum(spins)/(N*N))
	time.append(t)
	Mag.append(mag_of_config)
	t += 1


spins[spins == -1] = 0
Is.myplot(spins, "spins.pbm")


plt.plot(time, Mag, 'r')
plt.title('Magnetisation Vs steps')
plt.xlabel('Steps')
plt.ylabel('Magnetisation')
plt.show()














    










